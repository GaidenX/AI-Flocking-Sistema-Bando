﻿using UnityEngine;
using System.Collections;

public class ControladorPassaroLider : MonoBehaviour {

    private float rapidez = 100.0f;
    
	// Update is called once per frame
	void Update () {

        Vector3 posRelativa = Vector3.zero - transform.position;
        Quaternion rotation = Quaternion.LookRotation(posRelativa, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 1.0f * Time.deltaTime);

        transform.Translate(Vector3.forward * rapidez * Time.deltaTime);
	}
}
