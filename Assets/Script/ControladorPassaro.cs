﻿using UnityEngine;
using System.Collections;

public class ControladorPassaro : MonoBehaviour {

    public float velMinima = 20.0f; // velocidade minima de movimento
    public float velVirada = 20.0f; // velocidade de rotação
    public float frequenciaAleatoria = 20.0f; // Quantas vezes atualizar o valor em empurraoAleatorio (baseado no forcaAleatoria)
    public float forcaAleatoria = 20.0f; // Essa força cria, aleatoriamente, velocidades maiores e menores para movimentar o Pássaro.

    // Variáveis de Alinhamento
    public float forcaParaOrigem = 50.0f;
    public float dispersao = 100.0f;
    public float gravidade = 2.0f;

    // Variáveis de Separação
    // Permitem manter uma mínima distancia entre os Passaros
    public float raioParaEvitar = 50.0f;
    public float forcaParaEvitar = 20.0f;

    // Variáveis de Coesão
    // São utilizadas para manter uma mínima distancia em relação ao Lider Passaro (origem do bando)
    public float velocidadeParaSeguir = 4.0f;
    public float raioParaSeguir = 40.0f;

    // Estas variáveis controlam o movimento do Passaro
    private Transform origem;
    private Vector3 velocidade;
    private Vector3 velocidadeNormalizada;
    private Vector3 empurraoAleatorio;
    private Vector3 origemEmpurrao;

    // Cada Passaro precisa saber informações a respeito dos outros Passaros.
    // Essas duas variáveis armazenam informações da vizinhança.
    private Transform[] objetos;
    private ControladorPassaro[] outrosPassaros;

    private Transform componenteTransform;


	// Inicializando o Passaro
	void Start () {
        frequenciaAleatoria = 1.0f / frequenciaAleatoria;

        origem = transform.parent; // a origem é o pai dos Passaros.

        componenteTransform = transform;

        Component[] passarosTemporarios = null;

        // Pegar todos os individuos do bando a partir do Passaro Líder (Pai) do grupo
        if (transform.parent) {
            passarosTemporarios = transform.parent.GetComponentInChildren<ControladorPassaro>();
        }

        // Associe e armazene todos os objetos do bando
        objetos = new Transform[passarosTemporarios.Length];

        for (int i = 0; i < passarosTemporarios.Length; i++) {
            objetos[i] = passarosTemporarios[i].transform;
        }

        transform.parent = null;
        StartCoroutine(UpdateAleatorio());
	}

    IEnumerator UpdateAleatorio() {
        while (true) { 
            empurraoAleatorio = Random.insideUnitSphere * forcaAleatoria;
            yield return new WaitForSeconds(frequenciaAleatoria * Random.Range(-frequenciaAleatoria / 2.0f, frequenciaAleatoria / 2.0f));
        }
    }

    // Update is called once per frame
    void Update()
    {

        float rapidez = velocidade.magnitude;

        Vector3 velocidadeMedia = Vector3.zero;
        Vector3 posicaoMedia = Vector3.zero;

        float contador = 0.0f;
        float f = 0.0f;
        float d = 0.0f;

        Vector3 minhaPosicao = componenteTransform.position;
        Vector3 vetorForca;
        Vector3 destinoMedio;
        Vector3 velocidadeDesejada;

        for (int i = 0; i < objetos.Length; i++)
        {

            Transform oTransform = objetos[i];

            if (oTransform != componenteTransform)
            {
                Vector3 outraPosicao = oTransform.position;

                // Posição média para calcular Coesão
                posicaoMedia = posicaoMedia + outraPosicao;
                contador++;

                // Vetor direcional de um outro Passaro para este Passaro
                vetorForca = minhaPosicao - outraPosicao;

                // Magnitude desse Vetor direcional
                d = vetorForca.magnitude;

                // Adiciona um valor de empurrão se a magnitude, o comprimento do vetor,
                // é menor que o raioParaSeguir para o líder.
                if (d < raioParaSeguir)
                {
                    // Calcula-se velocidade do objeto baseado na distancia a ser evitada
                    // entre os Passaros, se a magnitude atual é menor que o raioParaEvitar especificado.
                    if (d < raioParaEvitar)
                    {
                        f = 1.0f - (d / raioParaEvitar);
                        if (d > 0)
                        {
                            velocidadeMedia = velocidadeMedia + (vetorForca / d) * f * forcaParaEvitar;
                        }
                    }
                }
            }
        }

        if (contador > 0)
        {
            // Calcula-se a velocidade media do bando (Alinhamento)
            velocidadeMedia = velocidadeMedia / contador;

            // Calcula-se o centro do bando (Coesão)
            destinoMedio = (posicaoMedia / contador) - minhaPosicao;
        }
        else
        {
            destinoMedio = Vector3.zero;
        }

        // Vetor direcional para o Líder
        vetorForca = origem.position - minhaPosicao;
        d = vetorForca.magnitude;
        f = d / dispersao;

        // Calcula-se a velocidade do Passaro para o Líder
        if (d > 0) { // Se este Passaro nao está no centro do bando
            origemEmpurrao = (vetorForca / d) * f * forcaParaOrigem;
        }

        if (rapidez < velMinima && rapidez > 0) {
            velocidadeMedia = (velocidadeMedia / rapidez) * velMinima;
        }

        velocidadeDesejada = velocidade;

        // Calcula-se a velocidade final:
        velocidadeDesejada = velocidadeDesejada - velocidadeDesejada * Time.deltaTime;
        velocidadeDesejada = velocidadeDesejada + empurraoAleatorio * Time.deltaTime;
        velocidadeDesejada = velocidadeDesejada + origemEmpurrao * Time.deltaTime;
        velocidadeDesejada = velocidadeDesejada + velocidadeMedia * Time.deltaTime;
        velocidadeDesejada = velocidadeDesejada + destinoMedio.normalized * gravidade * Time.deltaTime;

        // Velocidade final para rotacionar o Passaro
        velocidade = Vector3.RotateTowards(velocidade, velocidadeDesejada, velVirada * Time.deltaTime, 100.00f);

        componenteTransform.rotation = Quaternion.LookRotation(velocidade);

        // Move o Passaro baseado na velocidade calculada
        componenteTransform.Translate(velocidade * Time.deltaTime, Space.World); 
    }
}
